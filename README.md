# Visual recognition course - all assignments

1. Object Detection - simple ssd on penn fudan
2. Edge-Aware Person Segmentation - EfficientDet for edge-aware image segmentation on penn fudan
3. Normalizing Flows - bases on Real NVP (only first four points from task description)
